# TrailStash MapLibre Component

This repo provides a [Web
Component](https://developer.mozilla.org/en-US/docs/Web/API/Web_components) wrapping [MapLibre GL
JS](https://github.com/maplibre/maplibre-gl-js).

## Quickstart

Getting started with the TrailStash MapLibre Component can be as simple as this:

```html
<map-libre map-style="https://demotiles.maplibre.org/style.json"></map-libre>
<script src="https://www.unpkg.com/@trailstash/maplibre-component/dist/register.js"></script>
```

## Demo

Checkout a [remixable demo on glitch](https://maplibre-component.glitch.me/).

## API

### Supported HTML attributes

Properties changed via JavaScript are not reflected back to HTML attributes.

#### `map-style`

A URL to a MapLibre style JSON.

#### `zoom`

A number

#### `center`

Comma delmited x,y coordinates

#### `pitch`

A number

#### `bearing`

A number

#### `max-bounds`

Comma delmited xMin,yMin,xMax,yMax coordinates

#### `bounds`

Comma delmited xMin,yMin,xMax,yMax coordinates

### Supported element properties

#### `controls`

An arry of objects with the following keys:

- `type`: The name of a built-in Control in MapLibre GL JS or an instantiated Control
- `options`: an options object to pass to a Control specified by name in `type`
- `position`: the position to pass to `addControl`

Must be set before the component is added to the DOM.

#### `options`

A full [`MapOptions`](https://maplibre.org/maplibre-gl-js/docs/API/type-aliases/MapOptions/) object.

Must be set before the component is added to the DOM.

#### `mapStyle`

A URL to a MapLibre style JSON or a MapLibre style object.

Can be set after the component is added to the DOM.

#### `zoom`

A number.

Can be set after the component is added to the DOM.

#### `center`

A [`LngLatLike`](https://maplibre.org/maplibre-gl-js/docs/API/type-aliases/LngLatLike/)

Can be set after the component is added to the DOM.

#### `pitch`

A number

Can be set after the component is added to the DOM.

#### `bearing`

A number

Can be set after the component is added to the DOM.

#### `maxBounds`

A [`LngLatBoundsLike`](https://maplibre.org/maplibre-gl-js/docs/API/type-aliases/LngLatBoundsLike/)

Can be set after the component is added to the DOM.

#### `bounds`

A [`LngLatBoundsLike`](https://maplibre.org/maplibre-gl-js/docs/API/type-aliases/LngLatBoundsLike/)

Can be set after the component is added to the DOM.

## Example usage

### CDN

The scripts in `dist` include MapLibre GL in the bundle. This allows you to very quickly use the
component from a CDN.

```html
<map-libre map-style="https://demotiles.maplibre.org/style.json"></map-libre>
<script src="https://www.unpkg.com/@trailstash/maplibre-component/dist/register.js"></script>
```

### NPM

The NPM package has `maplibre-gl` as a `peerDependency`, allowing you
to provide the specific version of maplibre yourself.

#### Install package and MapLibre GL

```
npm i maplibre-gl @trailstash/maplibre-component
```

### import and register the component

```javascript
import MapLibre from "@trailstash/maplibre-component";

customElements.define("map-libre", MapLibre);
```

### Use the component

You can use the component in HTML similarly to the CDN example, or in this example, the component
is used via the JavaScript API:

```javascript
const maplibre = document.createElement("map-libre");
maplibre.mapStyle = "https://demotiles.maplibre.org/style.json";
document.body.appendChild(maplibre);
```

## Accessing the MapLibre object

This compnent doesn't try to hide the MapLibre
[`Map`](https://maplibre.org/maplibre-gl-js/docs/API/classes/Map/) object. It is available on the
component as `.map`. EG in the above JS example, `maplibre.map`.
